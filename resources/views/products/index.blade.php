@extends('layout') 
@section('content')

<h1>Products Index</h1>
<a name="" id="" class="btn btn-primary" href="{{ action('ProductsController@create') }}" role="button">CREATE</a>
<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>NAME</th>
            <th>STOCK</th>
            <th>PRICE</th>
            <th>ACTION</th>
        </tr>
    </thead>
    <tbody>

        @foreach($products as $product)
        <tr>
            <td scope="row">{{ $product->id }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->stock }}</td>
            <td>{{ $product->price }}</td>
            <td>
                <a name="" id="" class="btn btn-secondary" href="{{ action('ProductsController@edit', ['product' =>  $product ]) }}" role="button">EDIT</a>
                <a name="" id="" class="btn btn-danger" href="{{url('products/'.$product->id.'/destroy' ) }}" role="button">REMOVE</a>
                
           
            </td>
        </tr>
        <tr>
            @endforeach
    </tbody>
</table>
@endsection