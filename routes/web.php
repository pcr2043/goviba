<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get("/", "ProductsController@index")->name('products');
Route::get("products/create", "ProductsController@create");
Route::get("products/{product}/edit", "ProductsController@edit");

Route::post("products/store", "ProductsController@store");
Route::post("products/save", "ProductsController@update");
Route::get("products/{product}/destroy", "ProductsController@destroy");
