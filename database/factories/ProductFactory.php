<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->text,
        'price' => $faker->randomFloat(2, 0, 1000),
        'stock' => $faker->numberBetween(0,100),
    ];
});
