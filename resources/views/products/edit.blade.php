@extends('layout') 
@section('content')

    @if($product->id > 0)
        <form action="{{ action('ProductsController@update' ) }}" method="post">
        <input type="hidden"  name="id"  value="{{ $product->id }}"/>
    @else
        <form action="{{ action('ProductsController@store')  }}" method="post" >
    @endif

    @csrf

    <div class="row">
        <div class="col-md-12">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ $product->name }}" placeholder="Enter Name of Product">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="">Description</label>
                <textarea row="3" maxlength="250" class="form-control" name="description" id="description" 
                    placeholder="Enter the description">{{ $product->description }}</textarea>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Price</label>
                <input type="text" class="form-control" name="price" id="price" value="{{ $product->price }}" placeholder="Enter the Price">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Stock</label>
                <input type="text" class="form-control" name="stock" id="stock" value="{{ $product->stock }}" placeholder="Enter the Stock">
            </div>
        </div>
        <div class="col-md-12">
            <a href="{{ action('ProductsController@index') }}" role="button" class="btn btn-danger">CANCEL</a>
            <button type="submit" class="btn btn-primary">SAVE</button>
        </div>
    </div>
</form>
@endsection